#include <SFML/Graphics.hpp>
#include <string>
#include <cctype>
#include <iostream>

class InputText {
private:
	unsigned m_count = 0;
	sf::Text m_text;
	sf::RectangleShape m_shape;
	sf::Font * font;
	bool focuse = false;
	void appendChar(char);
	void removeChar();
public:
	//�����������
	InputText(sf::Vector2f position, sf::Vector2f size, unsigned count);
	~InputText();

	std::string getText();
	void clean();
	void setText(std::string);
	void setPosition(sf::Vector2f);
	void checkEvent(sf::RenderWindow &, sf::Event &);
	void draw(sf::RenderWindow &);
};

InputText::InputText(sf::Vector2f position, sf::Vector2f size, unsigned count) {
	m_count = count;
	
	m_shape.setOrigin(0, 0);
	m_shape.setSize(size);
	m_shape.setFillColor(sf::Color::White);
	m_shape.setOutlineColor(sf::Color::Black);
	m_shape.setOutlineThickness(1.f);
	font = new sf::Font;
	font->loadFromFile("./Helvetica.otf");
	m_text.setFont(*font);
	m_text.setCharacterSize(14);
	m_text.setColor(sf::Color::Black);
	m_text.setString("");
	setPosition(position);
}

InputText::~InputText() {
	delete font;
}

void InputText::removeChar() {
	if (m_text.getString().getSize() > 0) {
		m_text.setString(m_text.getString().substring(0, m_text.getString().getSize() - 1));
		//m_text.setOrigin(m_text.getGlobalBounds().width * 0.5f, m_text.getGlobalBounds().height * 0.5f);
		m_text.setOrigin(0, m_text.getCharacterSize() * 0.5f);
	}
}


void InputText::appendChar(char CH) {
	if (m_text.getString().getSize() < m_count) {
		m_text.setString(m_text.getString() + CH);
		m_text.setOrigin(0, m_text.getCharacterSize() * 0.5f);
	}
}

void InputText::setText(std::string text) {
	m_text.setString(text);
	m_text.setFont(*font);
	m_text.setColor(sf::Color::Black);
	m_text.setOrigin(0, m_text.getLocalBounds().height);
}

std::string InputText::getText() {
	return m_text.getString();
}

void InputText::clean() {
	m_text.setString("");
}

void InputText::draw(sf::RenderWindow & window) {
	window.draw(m_shape);
	window.draw(m_text);
}

void InputText::setPosition(sf::Vector2f position) {
	m_shape.setPosition(position);
	m_text.setPosition(m_shape.getPosition().x + 4, m_shape.getPosition().y + (m_shape.getLocalBounds().height - m_shape.getOutlineThickness() * 2) * 0.5f);
}

void InputText::checkEvent(sf::RenderWindow & window, sf::Event & events) {

	sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
	sf::Vector2f cursor = window.mapPixelToCoords(pixelPos);

	while (window.pollEvent(events)) {
		if (events.type == sf::Event::MouseButtonReleased) {
			if (m_shape.getGlobalBounds().contains(cursor.x, cursor.y)) {
				focuse = true;
				
			}
			else {
				focuse = false;
			}
		}

		if (events.type == sf::Event::TextEntered && focuse) {
			if (events.text.unicode == 8) {
				removeChar(); 
			}
			else if (events.text.unicode > 32 && events.text.unicode < 128) {
				if (std::isprint((char)events.text.unicode)) {
					appendChar(static_cast<char>(events.text.unicode));
				}
			}
		}
	}
	
}



int main() {
	InputText form(sf::Vector2f(100, 100), sf::Vector2f(100, 20), 10);//������� ������
	sf::RenderWindow  window(sf::VideoMode(800, 600), " Input form");
	sf::Event events;

	while (window.isOpen()) {
		window.clear(sf::Color::White);
		form.draw(window);//������������ ������

		form.checkEvent(window, events);//��������� ������� ������ �������

		while (window.pollEvent(events))
		{
			if (events.type == sf::Event::Closed) {
				window.close();
			}

		}
		
		std::cout << form.getText() << '\n';
		window.display();
	}
	return 0;
}